from django.core.management import BaseCommand
from pokemon.models import PokemonDetails, \
    PokemonAbilities,\
    PokemonForms, \
    PokemonMoves, \
    PokemonSpecies, \
    PokemonStats, \
    PokemonType

class Command(BaseCommand):
    help = "Reset pokemon data"

    get_pokemon_url = "https://pokeapi.co/api/v2/pokemon/"

    def handle(self, *args, **options):
        PokemonType.objects.all().delete()
        PokemonAbilities.objects.all().delete()
        PokemonSpecies.objects.all().delete()
        PokemonStats.objects.all().delete()
        PokemonForms.objects.all().delete()
        PokemonMoves.objects.all().delete()
        PokemonDetails.objects.all().delete()
        pass

    pass
