from django.contrib import admin
from .models import PokemonDetails
from django.urls import path
from django.conf.urls import include, url
from django.shortcuts import HttpResponse
from django.template.response import TemplateResponse
from django.utils.html import mark_safe


# Register your models here.

class PokemonDetailsAdmin(admin.ModelAdmin):

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
              path('update/', self.admin_site.admin_view(self.update_view), name='update_poke'),
        ]
        return custom_urls + urls

    def update_view(self, request, extra_context=None):
        context = self.admin_site.each_context(request)
        context["title"] = "Upload new file"
        return TemplateResponse(request, 'pokemondetails_form.html', context)
        pass

    def img_front_default_template(self, obj):
        value = "<img src='{value}'/ width='70' height='70'/>".format(value=obj.img_front_default)
        return mark_safe(value)
    img_front_default_template.short_description = 'Image Default'

    search_fields = ('name',);
    list_filter = ('date_created',);
    list_per_page = 10
    list_display = ('poke_id',
                    'img_front_default_template',
                    'name'
                    )
    pass


admin.site.register(PokemonDetails, PokemonDetailsAdmin)
