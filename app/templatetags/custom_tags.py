from django import template
import locale

locale.setlocale(locale.LC_ALL, '')

register = template.Library()

@register.filter(name="poke_id_format")
def str_num_format(string):
   if len(str(string)) < 3:
       if len(str(string)) == 1:
           return str("00") + str(string)
           pass
       return str("0") + str(string)
       pass

   return str(string)


@register.filter(name="progress_stats")
def progress_stats(val):
   if int(val) < 35:
       return "low"

   if int(val) >= 35 and int(val) < 65:
       return "medium"
       pass

   return "high"

@register.filter(name="to_nested_loops")
def to_nested_loops(arr):
    to_arr = []
    item = None
    for index,per in enumerate(arr):

        if len(to_arr) > 0 :
            print(item)
            item.append({
                "index" : index,
                "value" : per,
                "items" : []
            })
            item = item[-1]['items']
        else:
            to_arr.append({
                "index" : index,
                "value" : per,
                "items" : []
            })
            item = to_arr[-1]['items']
            pass


        pass
    return to_arr

    pass
