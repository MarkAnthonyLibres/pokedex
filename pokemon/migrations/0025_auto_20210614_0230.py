# Generated by Django 3.2.4 on 2021-06-13 18:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pokemon', '0024_auto_20210614_0216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pokemonmoves',
            name='accuracy',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='pokemonmoves',
            name='power',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='pokemonmoves',
            name='pp',
            field=models.IntegerField(default=0, null=True),
        ),
    ]
