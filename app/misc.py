from django.contrib.auth.views import TemplateView
from  django.views.generic.base import View
from django.shortcuts import redirect
from django.contrib import admin



class MyTemplateView(TemplateView):
    title = None
    context = None

    def set_context(self):
        pass

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context_value = admin.site.each_context(self.request)
        context["app_list"] = admin.site.get_app_list(self.request)
        context["title"] = self.title
        context.update(self.context or dict())
        context.update(self.set_context() or dict())
        context.update(context_value)
        return context
        pass

    pass
