(function(jQ) {

    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if (isMobile) {
       alert("BEST ON DESKTOP!");
    }

    const of_ajax = $.ajax;

    const buttons = jQuery("#pokemon-filter button");
    let show_details = jQuery(".pokemon-list .show_details");
    const loader = jQuery(".loader");
    const pokemon_cards_container = jQuery("#pokemon-cards")
    let filter_value = null;

    $.ajax = function(...args) {

        const request = of_ajax(...args);

        request.fail(function(e) {
            console.warn(e);
            alert('Error occurred');
        });

        request.done(setTimeout(function() {
            loader.addClass("hide");
        },500));

        return request;


    }

    filter_pokes = function() {

        const request = $.ajax({
            url: '/',
            headers: {"X-CSRFToken": jQuery.cookie("csrftoken")},
            type: 'POST',
            data : { 'value' : filter_value },
            beforeSend : function() {
                loader.removeClass("hide");
            },
        });

        request.then(function(res) {
            const elements = jQuery(res);
            pokemon_cards_container.html(elements);
            show_details = elements.find(".show_details");
            show_details.click(show_details_click);
        });

    }


    delete_poke = function() {

            const value = jQuery(this).attr("data-value");

            confirmation = confirm("Are you sure you want to permanently delete "+
            "this pokemon from your pokedex ?");

             if(!confirmation) return;

             const delete_request = $.ajax({
                url: '/details',
                headers: {"X-CSRFToken": jQuery.cookie("csrftoken")},
                type: 'DELETE',
                data : { 'value' : value },
                beforeSend : function() {
                    loader.removeClass("hide");
                },
            });

            delete_request.then(function() {
                filter_pokes();
                jQuery(".modal").modal("hide");
                alert("Successfully delete");
            })

        }

    show_details_click = function(e) {

        const value = jQuery(this).attr("data-value");

        const request = $.ajax({
            url: '/details',
            headers: {"X-CSRFToken": jQuery.cookie("csrftoken")},
            type: 'POST',
            data : { 'value' : value },
            beforeSend : function() {
                loader.removeClass("hide");
            },
        });


        request.then(function(res) {
            const that_modal = jQuery(res);
            that_modal.modal("show");
            that_modal.find("#delete_poke").off("click").on("click",delete_poke)
        });



    }

    show_details.click(show_details_click);
    buttons.click(function() {

        buttons.removeClass("-active");
        jQuery(this).addClass("-active");
        const value = jQuery(this).attr("data-value");
        filter_value = value;
        filter_pokes();

    });


})(jQuery)
