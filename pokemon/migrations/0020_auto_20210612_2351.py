# Generated by Django 3.2.4 on 2021-06-12 15:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pokemon', '0019_alter_pokemonspecies_species_poke_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='pokemonstats',
            name='pokemon',
            field=models.ForeignKey(blank=False, on_delete=django.db.models.deletion.CASCADE, to='pokemon.pokemondetails'),
        ),
        migrations.AddField(
            model_name='pokemontype',
            name='pokemon',
            field=models.ForeignKey(blank=False, on_delete=django.db.models.deletion.CASCADE, to='pokemon.pokemondetails'),
        ),
    ]
