from django.core.management import BaseCommand
from pokemon.models import PokemonDetails, \
    PokemonAbilities,\
    PokemonForms, \
    PokemonMoves, \
    PokemonSpecies, \
    PokemonStats, \
    PokemonType, MovesCombo
import requests
from django.core.files import File


class Command(BaseCommand):
    help = "Get the list of pokemons from the " \
           "pokeapi and save all to the database"

    get_pokemon_url = "https://pokeapi.co/api/v2/pokemon/"

    def __set_pokemon_type__(self, details, model):

        for per in details['types']:

            PokemonType.objects.create(
                name=per['type']['name'],
                slot=per['slot'],
                pokemon=model
            )

            pass

        pass

    def __set_pokemon_stats__(self, details, model):

        for per in details['stats']:
            PokemonStats.objects.create(
                name=per['stat']['name'],
                base_state=per['base_stat'],
                pokemon=model
            )
            pass

        pass

    def __set_evolution_chain__(self, species_details , model):

        if species_details['evolves_from_species']:

            of_send = requests.get(species_details['evolves_from_species']['url'])
            of_species_details = of_send.json()

            try :

                species = PokemonSpecies.objects.get(species_poke_id=of_species_details['id'])
                model_details_evolution = species.pokemon
                model.evolves_from = model_details_evolution
                model.save()

            except PokemonSpecies.DoesNotExist as e:
                # do something
                pass


            pass

        send = requests.get(species_details["evolution_chain"]["url"])
        evolution_details = send.json()

        model.evolution_chain = evolution_details["id"]
        model.save()

        pass

    def __set_pokemon_species__(self, details, model):

        model_species_details = PokemonSpecies.objects.create(
            name=details['species']['name'],
            pokemon=model
        )

        send = requests.get(details['species']['url'])
        species_details = send.json()

        model_species_details.species_poke_id = species_details['id']
        model_species_details.save()

        details = species_details['flavor_text_entries'][0]['flavor_text'] \
                    if len(species_details['flavor_text_entries']) > 0 else None

        form_description = species_details['form_descriptions'][0]['description'] \
                    if len(species_details['form_descriptions']) > 0 else None

        model.is_legendary = species_details['is_legendary']
        model.is_mythical = species_details['is_mythical']
        model.details = details
        model.form_details = form_description
        model.save()

        # get evolution chain
        self.__set_evolution_chain__(species_details, model)

        pass

    def __set_pokemon_moves__(self, details, model):
        for per in details['moves']:

            send = requests.get(per['move']['url'])
            move_details = send.json()

            details = move_details['flavor_text_entries'][0]['flavor_text'] \
                    if len(move_details['flavor_text_entries']) > 0 else None

            effect = move_details['effect_entries'][0]['short_effect'] \
                    if len(move_details['effect_entries']) > 0 else None

            poke_move = PokemonMoves.objects.create(
                name=move_details['name'],
                accuracy=move_details['accuracy'],
                pp=move_details['pp'],
                power=move_details['power'],
                pokemon=model,
                details=details,
                effect=effect
            )

            combo_list = move_details['contest_combos'] or dict()
            for type,combo in combo_list.items():

                use_before_list = combo['use_before'] or []
                use_after_list = combo['use_after'] or []

                for use_before in use_before_list:

                    MovesCombo.objects.create(
                        move_details=poke_move,
                        type=type,
                        name=use_before['name'],
                        is_before=True
                    )

                    pass

                for use_after in use_after_list:

                    MovesCombo.objects.create(
                        move_details=poke_move,
                        type=type,
                        name=use_after['name'],
                        is_before=False
                    )

                    pass

                pass


            pass
        pass

    def __set_pokemon_forms__(self, details, model):

        for per in details['forms']:

            PokemonForms.objects.create(
                name=per['name'],
                pokemon=model
            )

            pass

        pass

    def __set_pokemon_attribute__(self, details, model):

        for per in details['abilities']:

            PokemonAbilities.objects.create(
                name=per['ability']['name'],
                pokemon=model,
                is_hidden=per['is_hidden'],
                slot=per['slot']
            )

            pass

        pass

    def __set_pokemon_details__(self, details):

        return PokemonDetails.objects.create(
            name=details['name'],
            weight=details['weight'],
            height=details['height'],
            poke_id=details['id'],
            img_back_default= details['sprites']['back_default'],
            img_front_default = details['sprites']['front_default'],
            img_dream_world =  details['sprites']['other']['dream_world']['front_default'],
            img_official_artwork =  details['sprites']['other']['official-artwork']['front_default']
        )

        pass

    def __set_pokemon__(self, limit):
        PokemonDetails.objects.all().delete()
        send = requests.get(self.get_pokemon_url, params={"limit": limit})
        poke_list = send.json()
        for per in poke_list['results']:
            name = per['name']
            url = per['url']
            send = requests.get(url)
            poke_details = send.json()
            model_pokemon_details = self.__set_pokemon_details__(poke_details)
            self.__set_pokemon_attribute__(poke_details, model_pokemon_details)
            self.__set_pokemon_forms__(poke_details, model_pokemon_details)
            self.__set_pokemon_moves__(poke_details, model_pokemon_details)
            self.__set_pokemon_species__(poke_details, model_pokemon_details)
            self.__set_pokemon_stats__(poke_details, model_pokemon_details)
            self.__set_pokemon_type__(poke_details, model_pokemon_details)
            pass
        pass

    def add_arguments(self, parser):
        parser.add_argument('-c', '--count', type=int, default=151, help="Number of items to generate (default 151)")
        pass

    def handle(self, *args, **options):
        count = options['count']
        self.__set_pokemon__(count)
        pass

    pass
