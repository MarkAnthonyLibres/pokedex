from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.http import JsonResponse
from .misc import MyTemplateView
from pokemon.models import PokemonDetails, PokemonType
from django.http import QueryDict
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic.edit import UpdateView, CreateView
from .forms import PokemonDetailsForm
from django.contrib.auth.models import User
from pokemon.admin import PokemonDetailsAdmin
from django.urls import reverse
from django.contrib import messages


@method_decorator(login_required(login_url="admin_login"), name='dispatch')
class HomeView(MyTemplateView):
    template_name = "home.html"
    title = "Site Administration"

    def set_context(self, filter=None):
        poke_all = PokemonDetails.objects.all()
        poke_type = PokemonType.get_distinct_types()

        return {
            "poke_all": poke_all,
            "poke_type": poke_type
        }

        pass

    def post(self, request):
        params = request.POST
        poke_type = None
        if params['value']:
            poke_type = PokemonType.objects.filter(name=params['value'])
        else:
            poke_type = PokemonType.objects.all()
            pass

        return render(request, "home_filter.html", {
            "poke_type": poke_type
        })
        pass


class HomeModalView(MyTemplateView):

    def delete(self, request):
        params = QueryDict(request.body)
        poke_details = PokemonDetails.objects.get(id=params['value'])
        poke_details.delete()
        return JsonResponse({"message": True})
        pass

    def post(self, request):
        params = request.POST
        poke_details = PokemonDetails.objects.get(id=params['value'])
        return render(request, "poke_details.html", {
            "details": poke_details
        })
        pass

    pass


class OfLoginView(LoginView):
    redirect_authenticated_user = True
    template_name = "admin/login.html"

    def get_success_url(self):
        return "/"
        pass

    pass


class PokemonUpdateView(UpdateView):
    model = PokemonDetails
    form_class = PokemonDetailsForm
    template_name = 'pokemondetails_form.html'

    def has_permission(self, request):
        return request.user.is_active and request.user.is_staff

    def get_success_url(self):

        messages.success(self.request, 'Successfully update')

        pk = self.kwargs["pk"]
        return reverse("details-update", kwargs={"pk": pk})

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(PokemonUpdateView, self).get_context_data(**kwargs)

        print(context)

        data = {
            'opts': PokemonDetails._meta,
            'change': True,
            'add': False,
            'is_popup': False,
            'adminform' : PokemonDetailsAdmin,
            'save_as': False,
            'has_delete_permission': False,
            'has_add_permission': False,
            'has_change_permission': True,
            'has_view_permission' : True,
            'has_editable_inline_admin_formsets' : True
        }

        data.update(context)

        return data
        pass


class PokemonCreateView(CreateView):
    model = PokemonDetails
    form_class = PokemonDetailsForm
    template_name = 'pokemondetails_form.html'

    def has_permission(self, request):
        return request.user.is_active and request.user.is_staff

    def get_success_url(self):

        messages.success(self.request, 'Successfully added')
        return reverse("details-update")

    def get_context_data(self, **kwargs):
        context = super(PokemonCreateView, self).get_context_data(**kwargs)

        data = {
            'opts': PokemonDetails._meta,
            'change': True,
            'add': False,
            'is_popup': False,
            'adminform' : PokemonDetailsAdmin,
            'save_as': False,
            'has_delete_permission': False,
            'has_add_permission': False,
            'has_change_permission': True,
            'has_view_permission' : True,
            'has_editable_inline_admin_formsets' : True
        }

        data.update(context)

        return data
        pass
