from django.db import models
from datetime import date, datetime


# Create your models here.


class PokemonDetails(models.Model):
    id = models.AutoField(primary_key=True)
    poke_id = models.IntegerField(blank=False, null=False, unique=True, default=None)
    name = models.CharField(blank=False, null=False, max_length=250, unique=True)
    weight = models.IntegerField(blank=False, null=False)
    height = models.IntegerField(blank=False, null=False)
    date_created = models.DateTimeField(null=False, blank=False, default=datetime.now)
    img_back_default = models.ImageField(null=False, blank=False, default=None, max_length=300)
    img_front_default = models.ImageField(null=False, blank=False, default=None, max_length=300)
    img_dream_world = models.ImageField(null=False, blank=False, default=None, max_length=300)
    img_official_artwork = models.ImageField(null=False, blank=False, default=None, max_length=300)
    evolves_from = models.ForeignKey('self',null=True, blank=False,default=None,on_delete=models.DO_NOTHING)
    evolution_chain = models.IntegerField(blank=False, null=True, default=None)
    is_legendary = models.BooleanField(null=False, blank=False, default=False)
    is_mythical = models.BooleanField(null=False, blank=False, default=False)
    details = models.TextField(null=True, blank=False, default=None)
    form_details = models.TextField(null=True, blank=False, default=None)


    def get_forms(self):

        return PokemonDetails\
            .objects\
            .filter(evolution_chain=self.evolution_chain)\
            .order_by("evolves_from_id")

        pass

    def get_types(self):

        return PokemonType.objects.filter(pokemon=self.id)

        pass

    def get_abilities(self):
        return PokemonAbilities.objects.filter(pokemon=self.id)
        pass

    def get_stats(self):

        return PokemonStats.objects.filter(pokemon=self.id)

        pass


    def get_moves(self):
        return PokemonMoves.objects.filter(pokemon=self.id)
        pass

    pass




class PokemonAbilities(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(blank=False, null=False, max_length=250)
    pokemon = models.ForeignKey(PokemonDetails, blank=False, null=False, on_delete=models.CASCADE)
    date_created = models.DateTimeField(null=False, blank=False, default=datetime.now)
    is_hidden = models.BooleanField(default=False)
    slot = models.IntegerField(default=False, null=False)
    pass


class PokemonForms(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(blank=False, null=False, max_length=250)
    pokemon = models.ForeignKey(PokemonDetails, blank=False, null=False, on_delete=models.CASCADE)
    date_created = models.DateTimeField(null=False, blank=False, default=datetime.now)
    pass




class PokemonMoves(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(blank=False, null=False, max_length=250)
    accuracy = models.IntegerField(blank=False, null=True, default=0)
    pp = models.IntegerField(blank=False, null=True, default=0)
    power =  models.IntegerField(blank=False, null=True, default=0)
    details = models.TextField(blank=False, null=True,default=None)
    effect = models.TextField(blank=False, null=True,default=None)
    pokemon = models.ForeignKey(PokemonDetails, blank=False, null=False, on_delete=models.CASCADE)
    date_created = models.DateTimeField(null=False, blank=False, default=datetime.now)
    pass


class MovesCombo(models.Model):
    id = models.AutoField(primary_key=True)
    move_details = models.ForeignKey(PokemonMoves, blank=False, null=False, on_delete=models.CASCADE)
    type = models.CharField(blank=False, null=False, max_length=250)
    name = models.CharField(blank=False, null=False, max_length=250)
    is_before = models.BooleanField(blank=False, null=False, default=False)
    pass


class PokemonSpecies(models.Model):
    id = models.AutoField(primary_key=True)
    species_poke_id = models.IntegerField(blank=False, null=True, unique=True, default=None)
    name = models.CharField(blank=False, null=False, max_length=250)
    pokemon = models.ForeignKey(PokemonDetails, blank=False, null=False, on_delete=models.CASCADE)
    date_created = models.DateTimeField(null=False, blank=False, default=datetime.now)
    pass


class PokemonStats(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(blank=False, null=False, max_length=250)
    pokemon = models.ForeignKey(PokemonDetails,blank=False, null=False, on_delete=models.CASCADE)
    base_state = models.IntegerField(blank=False, null=False, default=0)
    date_created = models.DateTimeField(null=False, blank=False, default=datetime.now)
    pass


class PokemonType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(blank=False, null=False, max_length=250)
    pokemon = models.ForeignKey(PokemonDetails,blank=False, null=False, on_delete=models.CASCADE)
    slot = models.IntegerField(blank=False, null=False)
    date_created = models.DateTimeField(null=False, blank=False, default=datetime.now)

    @staticmethod
    def get_distinct_types():

        return PokemonType\
            .objects\
            .values('name')\
            .distinct()\
            .order_by("name")

        pass

    pass



