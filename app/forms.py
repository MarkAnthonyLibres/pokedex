from pokemon.models import PokemonDetails
from django import forms


class PokemonDetailsForm(forms.ModelForm):

    class Meta:
        model = PokemonDetails
        exclude = ('date_created',)
